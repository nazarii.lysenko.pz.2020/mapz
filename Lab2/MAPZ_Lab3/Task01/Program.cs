﻿using System;
using Task01.Classes;

namespace Task01
{
    class Program
    {
        static void Main(string[] args)
        {
            Garage garage = new Garage();
            garage.BuyCars();
            Console.WriteLine("List of cars: ");
            garage.PrintTable(garage.GetList());

            Console.WriteLine("\nList of car models (LINQ Select)");
            foreach (var item in garage.GetModels())
                Console.Write(item + " ");

            Console.WriteLine("\n\nList of only red cars (LINQ Where)");
            garage.PrintTable(garage.GetOnlyRed());

            Console.WriteLine("\nSorted list by model (LINQ Sort with IComparable)");
            garage.SortByModel();
            garage.PrintTable(garage.GetList());

            Console.WriteLine("\nSorted list by price (LINQ Sort with delegate)");
            garage.SortByPrice();
            garage.PrintTable(garage.GetList());

            Console.WriteLine("\nList of models and prices (anonymous type)");
            Console.Write(garage.GetModelsAndPrices());

            Console.WriteLine("\nCar models (List to array)");
            Console.Write(garage.GetModelsByArray());

            Console.WriteLine("\nNumber of cars by color (Dictionary)");
            Console.Write(garage.GetCarsByColor());

            Console.WriteLine("\nUnique colored car (Dictionary operation)");
            Console.Write(garage.GetUniqueColoredCars());
        }
    }
}
