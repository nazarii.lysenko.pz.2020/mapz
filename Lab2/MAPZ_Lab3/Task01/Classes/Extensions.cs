﻿using System;
using System.Linq;

namespace Task01.Classes
{
    public static class Extensions
    {
        public static string FormatPrice(this int num)
        {
            string toReturn = "$";
            int i = 0;
            while(num > 0)
            {
                if((i+1)%4==0)
                    toReturn += ",";
                toReturn += (num % 10).ToString();
                num /= 10;
                i++;
            }
            return new string(toReturn.Reverse().ToArray());
        }
    }
}
