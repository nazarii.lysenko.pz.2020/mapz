﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Xml.Serialization;

namespace Task01.Classes
{
    public class Garage
    {
        private List<Car> cars;
        Dictionary<string, List<Car>> carsByColor;
        public Garage()
        {
            carsByColor = new Dictionary<string, List<Car>>();
        }
        public List<Car> GetList()
        {
            return cars;
        }
        public void BuyCars()
        {
            XmlSerializer xml = new XmlSerializer(typeof(List<Car>));
            using (FileStream fs = File.Open(@"D:\lp\kurs 2 term 2\MAPZ\labs\mapz\Lab2\MAPZ_Lab3\Task01\bin\Debug\net5.0\Cars.xml", FileMode.Open))
            {
                cars = (List<Car>)xml.Deserialize(fs);
            }

            List<string> colors = cars.Select(x => x.Color).Distinct().ToList();
            foreach (var item in colors)
                carsByColor.Add(item, cars.Where(x => x.Color.Equals(item)).ToList());
        }
        public void PrintTable(List<Car> toPrint)
        {
            PrintLine();
            Console.WriteLine(String.Format("| {0,7} | {1,5} | {2,8} | {3,5} |", "Model", "Color", "Price", "Power"));
            PrintLine();
            foreach (var item in toPrint)
            {
                Console.WriteLine(item);
                PrintLine();
            }
        }
        private void PrintLine()
        {
            Console.WriteLine("+" + new String('-', 9) + "+" + new String('-', 7) + "+" + new String('-', 10) + "+" + new String('-', 7) + "+");
        }
        public List<string> GetModels()
        {
            return cars.Select(x => x.Model).Distinct().ToList();
        }   
        public List<Car> GetOnlyRed()
        {
            return cars.Where(x => x.Color == "Red").ToList();
        }
        public void SortByModel()
        {
            cars.Sort();
        }
        public void SortByPrice()
        {
            cars.Sort(delegate (Car x, Car y)
            {
                return y.Price.CompareTo(x.Price);
            });
        }
        public string GetModelsAndPrices()
        {
            string result = "";
            var carQuery =
                from car in cars
                select new { car.Model, car.Price };

            foreach (var item in carQuery)
                result += $"Model: {item.Model} \t - Price: {item.Price.FormatPrice()}\n";

            return result;
        }
        public string GetModelsByArray()
        {
            string result = "";
            foreach (var item in cars.ToArray())
                result += "Model: " + item.Model + "\n";
            return result;
        }
        public string GetCarsByColor()
        {
            string result = "";

            foreach (var item in carsByColor)
                result += $"There are {item.Value.Count} {item.Key} cars\n";
            return result;
        }
        public string GetUniqueColoredCars()
        {
            string result = "";
            var numberQuery = carsByColor.Where(x => x.Value.Count == 1);
            foreach (var item in numberQuery)
                result += $"There is only 1 {item.Key} car: {item.Value.First().ToString(1)}\n";
            return result;
        }
    }
}
