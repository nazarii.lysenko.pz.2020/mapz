﻿using System;

namespace Task01.Classes
{
    public class Car: IComparable<Car>
    {
        public string Model { get; set; }
        public string Color { get; set; }
        public int Price { get; set; }
        public int Power { get; set; }
        public Car()
        {
            Model = "empty";
            Color = "empty";
            Price = -1;
            Power = -1;
        }
        public Car(string uModel, string uColor, int uPrice, int uPower)
        {
            Model = uModel;
            Color = uColor;
            Price = uPrice;
            Power = uPower;
        }
        public Car(Car prev)
        {
            Model = prev.Model;
            Color = prev.Color;
            Price = prev.Price;
            Power = prev.Power;
        }
        public override string ToString()
        {
            return String.Format("| {0,7} | {1,5} | {2,8} | {3,5} |", Model, Color, Price.FormatPrice(), Power);
        }
        public string ToString(int a)
        {
            return $"{Model}, {Price.FormatPrice()}, {Power}hp";
        }
        public int CompareTo(Car other)
        {
            if (other == null)
                return 1;
            return this.Model.CompareTo(other.Model);
        }
    }
}
