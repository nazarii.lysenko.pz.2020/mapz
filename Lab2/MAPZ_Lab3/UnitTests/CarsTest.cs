using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Collections.Generic;
using Task01.Classes;

namespace UnitTests
{
    [TestClass]
    public class CarsTest
    {
        [TestMethod]
        public void TestUniqueColor()
        {
            string expected = "There is only 1 Blue car: Toyota, 43,000$, 260hp\nThere is only 1 Grey car: Ford, 30,000$, 150hp\n";
            string actual = "";
            Garage garage = new Garage();
            garage.BuyCars();
            actual = garage.GetUniqueColoredCars();
            Assert.AreEqual(expected, actual, "Not equal");
        }
        [TestMethod]
        public void TestCarsByColor()
        {
            string expected = "There are 2 Black cars\nThere are 3 Red cars\nThere are 2 White cars\nThere are 1 Blue cars\nThere are 1 Grey cars\n";
            string actual = "";
            Garage garage = new Garage();
            garage.BuyCars();
            actual = garage.GetCarsByColor();
            Assert.AreEqual(expected, actual, "Not equal");
        }
        [TestMethod]
        public void TestModels()
        {
            List<string> expected = new List<string>();
            string expectedString = "";
            string actualString = "";
            expected.Add("Audi");
            expected.Add("Ford");
            expected.Add("Tesla");
            expected.Add("Mazda");
            expected.Add("Toyota");
            expected.Add("Chevy");
            expected.Add("Ferrari");
            expected.Add("BMW");
            Garage garage = new Garage();
            garage.BuyCars();
            List<string> actual = garage.GetModels();
            foreach (var item in expected)
                expectedString += item;
            System.Console.WriteLine();
            foreach (var item in actual)
                actualString += item;
            Assert.AreEqual(expectedString, actualString, "Not equal");
        }
    }
}
