﻿using System.Collections.Generic;

namespace SpacePirates.Classes
{
    public sealed class Stronghold
    {
        private Stronghold(){}
        private static Stronghold instance;

        public static Stronghold GetInstance()
        {
            if(instance == null)
                instance = new Stronghold();
            return instance;
        }
        public void UpgradeBlaster(Spaceship spaceship)
        {
            spaceship.BlasterLvl++;
        }
        public void UpgradeSpaceship(Spaceship spaceship)
        {
            spaceship.SpaceshipLvl++;
        }
        public void UpgradeCrew(Spaceship spaceship)
        {
            spaceship.CrewLvl++;
        }
    }
}
