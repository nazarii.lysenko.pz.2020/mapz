﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Windows.Controls;
using System.Windows.Media.Imaging;

namespace SpacePirates.Classes
{
    public interface IObserver
    {
        void Update(ILevel subject);
    }
    public interface ILevel
    {
        void Attach(IObserver observer);

        void Detach(IObserver observer);

        void Notify();
    }
    public class Level : ILevel
    {
        public int lvl { get; private set; } = 0;

        private List<IObserver> _observers = new List<IObserver>();

        public void Attach(IObserver observer)
        {
            this._observers.Add(observer);
        }

        public void Detach(IObserver observer)
        {
            this._observers.Remove(observer);
        }

        public void Notify()
        {
            foreach (var observer in _observers)
                observer.Update(this);
        }

        public void IncreaseLvl()
        {
            this.lvl++;
            this.Notify();
        }
    }
    public class BlasterObserver : IObserver
    {
        public TextBlock tb_blaster{ get; set; }
        public Image img_blaster{ get; set; }
        public BlasterObserver(TextBlock tb, Image img)
        {
            tb_blaster = tb;
            img_blaster = img;
        }
        public void Update(ILevel subject)
        {
            tb_blaster.Text = (subject as Level).lvl.ToString();
            if((subject as Level).lvl >= 5 && (subject as Level).lvl < 10)
                img_blaster.Source = new BitmapImage(new Uri(Path.GetFullPath(@"../../../middle_blaster.png"), UriKind.Absolute));  
            if((subject as Level).lvl >= 10)
                img_blaster.Source = new BitmapImage(new Uri(Path.GetFullPath(@"../../../best_blaster.png"), UriKind.Absolute));  
        }
    }
    public class SpaceshipObserver : IObserver
    {
        public TextBlock tb_spaceship { get; set; }
        public Image img_spaceship { get; set; }

        public SpaceshipObserver(TextBlock tb, Image img_spaceship)
        {
            tb_spaceship = tb;
            this.img_spaceship = img_spaceship;
        }
        public void Update(ILevel subject)
        {
            tb_spaceship.Text = (subject as Level).lvl.ToString();
            if ((subject as Level).lvl >= 5 && (subject as Level).lvl < 10)
                img_spaceship.Source = new BitmapImage(new Uri(Path.GetFullPath(@"../../../middle_ship.png"), UriKind.Absolute));
            if ((subject as Level).lvl >= 10)
                img_spaceship.Source = new BitmapImage(new Uri(Path.GetFullPath(@"../../../best_ship.png"), UriKind.Absolute));
        }
    }
    public class CrewObserver : IObserver
    {
        public TextBlock tb_crew { get; set; }
        public Image img_crew { get; set; }

        public CrewObserver(TextBlock tb, Image img)
        {
            tb_crew = tb;
            img_crew = img;
        }
        public void Update(ILevel subject)
        {
            tb_crew.Text = (subject as Level).lvl.ToString();
            if ((subject as Level).lvl >= 5 && (subject as Level).lvl < 10)
                img_crew.Source = new BitmapImage(new Uri(Path.GetFullPath(@"../../../middle_crew.png"), UriKind.Absolute));
            if ((subject as Level).lvl >= 10)
                img_crew.Source = new BitmapImage(new Uri(Path.GetFullPath(@"../../../best_crew.png"), UriKind.Absolute));
        }
    }
}
