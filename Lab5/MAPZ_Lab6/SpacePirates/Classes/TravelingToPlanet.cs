﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Media;
using System.Windows.Media.Imaging;

namespace SpacePirates.Classes
{
    abstract class TravelingToPlanet
    {
        protected void ButtonClick(Popup popup, Button btn, RoutedEventHandler travelClick)
        {
            popup.IsOpen = false;
            btn.Click -= new RoutedEventHandler(travelClick);
        }
        protected void VisibilityChange(Grid main, Grid planet, Image img_planet)
        {
            main.Visibility = Visibility.Hidden;
            planet.Visibility = Visibility.Visible;
            img_planet.Visibility = Visibility.Visible;
        }
        protected abstract void SetDisaster(Grid planet);
        protected void CreatePlants(Grid planet)
        {
            (Window.GetWindow(App.Current.MainWindow) as MainWindow).CreatePlantsInterface(planet);
        }
        public void TemplateMethod(Popup popup, Button btn, RoutedEventHandler travelClick, Grid main, Grid planet, Image img_planet)
        {
            this.ButtonClick(popup, btn, travelClick);
            this.VisibilityChange(main, planet, img_planet);
            this.SetDisaster(planet);
            this.CreatePlants(planet);
        }
    }
    class TravelingToPandora : TravelingToPlanet
    {
        protected override void SetDisaster(Grid planet)
        {
            if (planet.Children.Count == 2)
            {
                for (int i = 0; i < 35; i++)
                {
                    Image flake = new Image();
                    flake.Source = new BitmapImage(new Uri(Path.GetFullPath(@"../../../snow.png"), UriKind.Absolute));
                    flake.Width = 30;
                    flake.Margin = new Thickness(new Random().Next(-800, 850), new Random().Next(-400, 50), 0, 0);
                    planet.Children.Insert(0, flake);
                }

            }
        }
    }
    class TravelingToMars : TravelingToPlanet
    {
        protected override void SetDisaster(Grid planet)
        {
            if (planet.Children.Count == 2)
            {
                Image thunder1 = new Image();
                thunder1.Source = new BitmapImage(new Uri(Path.GetFullPath(@"../../../thunder.png"), UriKind.Absolute));
                thunder1.Width = 300;
                thunder1.Margin = new Thickness(-400, -120, 0, 0);
                planet.Children.Insert(0, thunder1);
                Image thunder2 = new Image();
                thunder2.Source = new BitmapImage(new Uri(Path.GetFullPath(@"../../../thunder.png"), UriKind.Absolute));
                thunder2.Width = 300;
                thunder2.Margin = new Thickness(400, -90, 0, 0);
                thunder2.RenderTransformOrigin = new Point(0.5, 0.5);
                ScaleTransform flipTrans = new ScaleTransform();
                flipTrans.ScaleX = -1;
                thunder2.RenderTransform = flipTrans;
                planet.Children.Insert(0, thunder2);
            }
        }
    }
    class TravelingToSirius : TravelingToPlanet
    {
        protected override void SetDisaster(Grid planet)
        {
            if (planet.Children.Count == 2)
            {
                Image wave1 = new Image();
                wave1.Source = new BitmapImage(new Uri(Path.GetFullPath(@"../../../wave.png"), UriKind.Absolute));
                wave1.Width = 350;
                wave1.Margin = new Thickness(-450, 200, 0, 0);
                planet.Children.Insert(0, wave1);
                Image wave2 = new Image();
                wave2.Source = new BitmapImage(new Uri(Path.GetFullPath(@"../../../wave.png"), UriKind.Absolute));
                wave2.Width = 350;
                wave2.Margin = new Thickness(450, 200, 0, 0);
                wave2.RenderTransformOrigin = new Point(0.5, 0.5);
                ScaleTransform flipTrans = new ScaleTransform();
                flipTrans.ScaleX = -1;
                wave2.RenderTransform = flipTrans;
                planet.Children.Add(wave2);
            }
        }
    }
    class TravelingToBlake : TravelingToPlanet
    {
        protected override void SetDisaster(Grid planet)
        {
            if (planet.Children.Count == 2)
            {
                Image wave1 = new Image();
                wave1.Source = new BitmapImage(new Uri(Path.GetFullPath(@"../../../volcano.png"), UriKind.Absolute));
                wave1.Width = 500;
                wave1.Margin = new Thickness(0, -100, 0, 0);
                planet.Children.Insert(0, wave1);
            }
        }
    }
    class TravelingToOreo : TravelingToPlanet
    {
        protected override void SetDisaster(Grid planet)
        {
            if (planet.Children.Count == 2)
            {
                Image rain = new Image();
                rain.Source = new BitmapImage(new Uri(Path.GetFullPath(@"../../../rain.png"), UriKind.Absolute));
                rain.Width = 1000;
                rain.Stretch = Stretch.Fill;
                rain.Margin = new Thickness(0, 0, 0, 0);
                planet.Children.Insert(0, rain);

                Image puddle1 = new Image();
                puddle1.Source = new BitmapImage(new Uri(Path.GetFullPath(@"../../../puddle.png"), UriKind.Absolute));
                puddle1.Width = 270;
                puddle1.Margin = new Thickness(-450, 300, 0, 0);
                planet.Children.Insert(0, puddle1);
                Image puddle2 = new Image();
                puddle2.Source = new BitmapImage(new Uri(Path.GetFullPath(@"../../../puddle.png"), UriKind.Absolute));
                puddle2.Width = 270;
                puddle2.Margin = new Thickness(450, 300, 0, 0);
                puddle2.RenderTransformOrigin = new Point(0.5, 0.5);
                ScaleTransform flipTrans = new ScaleTransform();
                flipTrans.ScaleX = -1;
                puddle2.RenderTransform = flipTrans;
                planet.Children.Add(puddle2);
            }
        }
    }
}
