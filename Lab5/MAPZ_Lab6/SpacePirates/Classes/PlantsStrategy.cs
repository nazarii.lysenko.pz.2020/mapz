﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;

namespace SpacePirates.Classes
{
    class Context
    {
        private IStrategy _strategy;

        public Context()
        { }

        public Context(IStrategy strategy)
        {
            this._strategy = strategy;
        }

        public void SetStrategy(IStrategy strategy)
        {
            this._strategy = strategy;
        }

        public void AddPlant(PlantsManager pm, string name, string path, Image img)
        {
            this._strategy.CreatePlant(pm, name, path, img);
        }
    }
    public interface IStrategy
    {
        void CreatePlant(PlantsManager pm, string name, string path, Image img);
    }
    class AngryStrategy : IStrategy
    {
        public void CreatePlant(PlantsManager pm, string name, string path, Image img)
        {
            Random random = new Random();
            pm.AddPlant(random.Next(150, 250), random.Next(-700, 700), random.Next(150, 180), name, path, new AngryState(img));
        }
    }
    class NormalStrategy : IStrategy
    {
        public void CreatePlant(PlantsManager pm, string name, string path, Image img)
        {
            Random random = new Random();
            pm.AddPlant(random.Next(150, 250), random.Next(-700, 700), random.Next(80, 120), name, path, new NormalState(img));
        }
    }
}
