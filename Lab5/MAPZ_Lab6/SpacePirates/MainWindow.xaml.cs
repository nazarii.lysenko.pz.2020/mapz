﻿using SpacePirates.Classes;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;

namespace SpacePirates
{
    public partial class MainWindow : Window
    {
        public Stronghold citadel;
        Spaceship spaceship;
        TextBlock textBlock = new TextBlock();
        TextBlock chestBlock = new TextBlock();
        string[] names = { "Frosty", "Cold_Kawun", "Abobus", "Capusta" };
        List<Image> images = new List<Image>();
        Grid[] planetGrids;
        Image[] planetImages;
        Planet planet;
        Squadron crew = new Squadron();
        public MainWindow()
        {
            InitializeComponent();

            citadel = Stronghold.GetInstance();
            spaceship = Spaceship.GetInstance();
            planetGrids = new Grid[] { gr_pandora, gr_mars, gr_oreo, gr_sirius, gr_blake };
            planetImages = new Image[] { img_pandora, img_oreo, img_mars, img_sirius, img_blake };

            textBlock.VerticalAlignment = VerticalAlignment.Top;
            textBlock.HorizontalAlignment = HorizontalAlignment.Right;
            textBlock.FontSize = 20;
            textBlock.Foreground = Brushes.WhiteSmoke;

            chestBlock.VerticalAlignment = VerticalAlignment.Top;
            chestBlock.HorizontalAlignment = HorizontalAlignment.Left;
            chestBlock.FontSize = 20;
            chestBlock.Foreground = Brushes.WhiteSmoke;
            chestBlock.Margin = new Thickness(0, 70, 0, 0);

            Squadron squadrone1 = new Squadron();
            Squadron squadrone2 = new Squadron();
            Squadron squadrone3 = new Squadron();

            squadrone1.Add(new Member());
            squadrone1.Add(new Member());

            squadrone2.Add(new Member());
            squadrone2.Add(new Member());
            squadrone2.Add(new Member());

            squadrone3.Add(new Member());

            crew.Add(squadrone1);
            crew.Add(squadrone2);
            crew.Add(squadrone3);

            spaceship.BlasterLvl.Attach(new BlasterObserver(tb_blaster, img_blaster));
            spaceship.SpaceshipLvl.Attach(new SpaceshipObserver(tb_spaceship, img_ship));
            spaceship.crew.level.Attach(new CrewObserver(tb_crew, img_crew));
        }
        public void CreatePlantsInterface(Grid planetGrid)
        {
            List<int> numberOfPlants = new List<int>();
            string[] paths = { @"../../../frost.png", @"../../../frost_watermelon.png", @"../../../NUT.png", @"../../../capusta.png" };
            Random random = new Random();
            PlantsManager plantsManager = new PlantsManager();
            Context context = new Context();
            for (int i = 0; i < 4; i++)
                numberOfPlants.Add(random.Next(1, 4));
            for (int j = 0; j < numberOfPlants.Count; j++)
                for (int i = 0; i < numberOfPlants[j]; i++)
                {
                    if (random.Next(4) % 4 != 0)
                        context.SetStrategy(new NormalStrategy());
                    else
                        context.SetStrategy(new AngryStrategy());
                    context.AddPlant(plantsManager, names[j], paths[j], img_fire);
                }

            planetGrid.Children.Add(textBlock);
            planetGrid.Children.Add(chestBlock);
            foreach (var item in plantsManager.plants)
            {
                Image plant = new Image();
                plant.Source = new BitmapImage(new Uri(Path.GetFullPath(item.type.ImagePath), UriKind.Absolute));
                plant.Width = item.type.Size;
                plant.Margin = new Thickness(item.MarginLeft, item.MarginTop, 0, 0);
                plant.Name = item.type.Name;
                plant.MouseDown += new MouseButtonEventHandler(item.type.State.GetActive);
                plant.Cursor = Cursors.Hand;
                images.Add(plant);
                planetGrid.Children.Add(plant);
            }


            for (int i = 0; i < numberOfPlants.Count; i++)
            {
                textBlock.Text += plantsManager.plants.Where(plant => plant.type.Name == names[i]).FirstOrDefault().type.Name + ": " + numberOfPlants[i] + "\n";
            }
            Client client = new Client();
            FoodDecorator fd = new FoodDecorator(new Chest());
            fd.fillChest();
            chestBlock.Text += client.GetChestsInfo(fd, planet.Chests / 3) + "\n";

            ArmorDecorator ad = new ArmorDecorator(fd);
            ad.fillChest();
            chestBlock.Text += client.GetChestsInfo(ad, planet.Chests / 3) + "\n";

            TreasureDecorator td = new TreasureDecorator(ad);
            td.fillChest();
            chestBlock.Text += client.GetChestsInfo(td, planet.Chests / 3);

        }
        private void ShowFlames(object sender, RoutedEventArgs e)
        {

        }
        private void RemovePlants(Grid planetGrid)
        {
            planetGrid.Children.Remove(textBlock);
            textBlock.Text = "";
            chestBlock.Text = "";
            if (planetGrid.Children.Count > 7)
                while (images.Count > 0)
                {
                    if (planetGrid.Children.Contains(images[0]))
                    {
                        planetGrid.Children.Remove(images[0]);
                        images.Remove(images[0]);
                    }
                    else
                        break;
                }
        }
        #region Spaceship Upgrades
        private void UpgradeBlaster_Click(object sender, RoutedEventArgs e)
        {
            StrongholdFacade.UpgradeBlasters();
        }
        private void UpgradeSpaceship_Click(object sender, RoutedEventArgs e)
        {
            StrongholdFacade.UpgradeSpaceship();
        }
        private void UpgradeCrew_Click(object sender, RoutedEventArgs e)
        {
            StrongholdFacade.UpgradeCrew();
        }
        private void GetCrewInfo_Click(object sender, RoutedEventArgs e)
        {
            tb_popup.Text = spaceship.crew.GetCrewInfo(crew);
            btn_travel.Visibility = Visibility.Hidden;
            PlanetPopup.IsOpen = true;

        }
        #endregion
        #region Planet Travels
        private void PandoraTravelClick(object sender, RoutedEventArgs e)
        {
            TravelingToPandora ttp = new TravelingToPandora();
            ttp.TemplateMethod(PlanetPopup, btn_travel, new RoutedEventHandler(PandoraTravelClick),
                gr_main, gr_pandora, img_pandora);
        }
        private void MarsTravelClick(object sender, RoutedEventArgs e)
        {
            TravelingToMars ttm = new TravelingToMars();
            ttm.TemplateMethod(PlanetPopup, btn_travel, new RoutedEventHandler(MarsTravelClick),
                gr_main, gr_mars, img_mars);

        }
        private void SiriusTravelClick(object sender, RoutedEventArgs e)
        {
            TravelingToSirius tts = new TravelingToSirius();

            tts.TemplateMethod(PlanetPopup, btn_travel, new RoutedEventHandler(SiriusTravelClick),
                gr_main, gr_sirius, img_sirius);
        }
        private void BlakeTravelClick(object sender, RoutedEventArgs e)
        {
            TravelingToBlake ttb = new TravelingToBlake();

            ttb.TemplateMethod(PlanetPopup, btn_travel, new RoutedEventHandler(BlakeTravelClick),
                gr_main, gr_blake, img_blake);
        }
        private void OreoTravelClick(object sender, RoutedEventArgs e)
        {
            TravelingToOreo tto = new TravelingToOreo();

            tto.TemplateMethod(PlanetPopup, btn_travel, new RoutedEventHandler(OreoTravelClick),
                gr_main, gr_oreo, img_oreo);
        }
        #endregion
        #region Planet Info
        private void Pandora_Click(object sender, RoutedEventArgs e)
        {
            HideMarket();
            planet = new Planet(new Pandora());
            RemoveClickEvent();
            btn_travel.Click += new RoutedEventHandler(PandoraTravelClick);
            tb_popup.Text = planet.GetInfo();
            PlanetPopup.IsOpen = true;
        }
        private void Oreo_Click(object sender, RoutedEventArgs e)
        {
            HideMarket();
            planet = new Planet(new Oreo());
            RemoveClickEvent();
            btn_travel.Click += new RoutedEventHandler(OreoTravelClick);
            tb_popup.Text = planet.GetInfo();
            PlanetPopup.IsOpen = true;
        }
        private void Mars_Click(object sender, RoutedEventArgs e)
        {
            HideMarket();
            planet = new Planet(new Mars());
            RemoveClickEvent();
            btn_travel.Click += new RoutedEventHandler(MarsTravelClick);
            tb_popup.Text = planet.GetInfo();
            PlanetPopup.IsOpen = true;
        }
        private void Sirius_Click(object sender, RoutedEventArgs e)
        {
            HideMarket();
            planet = new Planet(new Sirius());
            RemoveClickEvent();
            btn_travel.Click += new RoutedEventHandler(SiriusTravelClick);
            tb_popup.Text = planet.GetInfo();
            PlanetPopup.IsOpen = true;
        }
        private void Blake_Click(object sender, RoutedEventArgs e)
        {
            HideMarket();
            planet = new Planet(new Blake());
            RemoveClickEvent();
            btn_travel.Click += new RoutedEventHandler(BlakeTravelClick);
            tb_popup.Text = planet.GetInfo();
            PlanetPopup.IsOpen = true;
        }
        #endregion
        private void Stronghold_Click(object sender, RoutedEventArgs e)
        {
            sp_upgrades.Visibility = Visibility.Visible;
            sp_images.Visibility = Visibility.Visible;
            tb_title.Visibility = Visibility.Visible;
        }
        #region Support Methods
        private void HideMarket()
        {
            sp_upgrades.Visibility = Visibility.Hidden;
            sp_images.Visibility = Visibility.Hidden;
            tb_title.Visibility = Visibility.Hidden;
        }
        private void Cancel_Click(object sender, RoutedEventArgs e)
        {
            PlanetPopup.IsOpen = false;
            btn_travel.Visibility = Visibility.Visible;
        }
        private void Back_Click(object sender, RoutedEventArgs e)
        {
            PlanetPopup.IsOpen = false;
            gr_main.Visibility = Visibility.Visible;
            img_fire.Visibility = Visibility.Hidden;
            foreach (var item in planetGrids)
            {
                item.Visibility = Visibility.Hidden;
                item.Children.Remove(textBlock);
                item.Children.Remove(chestBlock);
                RemovePlants(item);
            }
            foreach (var item in planetImages)
            {
                item.Visibility = Visibility.Hidden;
            }
        }
        private void RemoveClickEvent()
        {
            btn_travel.Click -= new RoutedEventHandler(PandoraTravelClick);
            btn_travel.Click -= new RoutedEventHandler(MarsTravelClick);
            btn_travel.Click -= new RoutedEventHandler(SiriusTravelClick);
            btn_travel.Click -= new RoutedEventHandler(BlakeTravelClick);
            btn_travel.Click -= new RoutedEventHandler(OreoTravelClick);

        }
        #endregion
    }
}


