﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SpacePirates.Classes
{
    abstract class Unit
    {
        public int HP { get; private set; }
        public int Damage { get; private set; }

        public Unit(int HP, int Damage)
        {
            this.HP = HP;
            this.Damage = Damage;
        }
        public abstract Unit Clone();
        public abstract string Attack();
    }
    class CannonPirate : Unit
    {
        public CannonPirate(int HP, int Damage) : base(HP, Damage) { }
        public override Unit Clone()
        {
            return new CannonPirate(HP, Damage);
        }
        public override string Attack()
        {
            return "Boom! Boom!";
        }
    }
    class SwordPirate : Unit
    {
        public SwordPirate(int HP, int Damage) : base(HP, Damage) { }
        public override Unit Clone()
        {
            return new SwordPirate(HP, Damage);
        }
        public override string Attack()
        {
            return "Swish! Swish!";
        }
    }
}
