﻿namespace SpacePirates.Classes
{
    public static class StrongholdFacade
    {
        public static void UpgradeSpaceship()
        {
            Stronghold sh = Stronghold.GetInstance();
            Spaceship sp = Spaceship.GetInstance();

            sh.UpgradeSpaceship(sp);
        }   
        public static void UpgradeBlasters()
        {
            Stronghold sh = Stronghold.GetInstance();
            Spaceship sp = Spaceship.GetInstance();

            sh.UpgradeBlaster(sp);
        }
        public static void UpgradeCrew()
        {
            Stronghold sh = Stronghold.GetInstance();
            Spaceship sp = Spaceship.GetInstance();

            sh.UpgradeCrew(sp);
        }
    }
}
