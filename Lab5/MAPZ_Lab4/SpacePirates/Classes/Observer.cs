﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;

namespace SpacePirates.Classes
{
    public interface IObserver
    {
        void Update(ILevel subject);
    }
    public interface ILevel
    {
        void Attach(IObserver observer);

        void Detach(IObserver observer);

        void Notify();
    }
    public class Level : ILevel
    {
        public int lvl { get; private set; } = 0;

        private List<IObserver> _observers = new List<IObserver>();

        public void Attach(IObserver observer)
        {
            this._observers.Add(observer);
        }

        public void Detach(IObserver observer)
        {
            this._observers.Remove(observer);
        }

        public void Notify()
        {
            foreach (var observer in _observers)
                observer.Update(this);
        }

        public void IncreaseLvl()
        {
            this.lvl++;
            this.Notify();
        }
    }
    class BlasterObserver : IObserver
    {
        public TextBlock tb_blaster{ get; set; }
        public BlasterObserver(TextBlock tb)
        {
            tb_blaster = tb;
        }
        public void Update(ILevel subject)
        {
            tb_blaster.Text = (subject as Level).lvl.ToString();
        }
    }
    class SpaceshipObserver : IObserver
    {
        public TextBlock tb_spaceship { get; set; }
        public SpaceshipObserver(TextBlock tb)
        {
            tb_spaceship = tb;
        }
        public void Update(ILevel subject)
        {
            tb_spaceship.Text = (subject as Level).lvl.ToString();
        }
    }
    class CrewObserver : IObserver
    {
        public TextBlock tb_crew { get; set; }
        public CrewObserver(TextBlock tb)
        {
            tb_crew = tb;
        }
        public void Update(ILevel subject)
        {
            tb_crew.Text = (subject as Level).lvl.ToString();
        }
    }
}
