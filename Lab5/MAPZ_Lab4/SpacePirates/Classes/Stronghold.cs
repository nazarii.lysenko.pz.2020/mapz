﻿namespace SpacePirates.Classes
{
    public sealed class Stronghold
    {
        private Stronghold(){}
        private static Stronghold instance;

        public static Stronghold GetInstance()
        {
            if(instance == null)
                instance = new Stronghold();
            return instance;
        }
        public void UpgradeBlaster(Spaceship spaceship)
        {
            spaceship.BlasterLvl.IncreaseLvl();
        }
        public void UpgradeSpaceship(Spaceship spaceship)
        {
            spaceship.SpaceshipLvl.IncreaseLvl();
        }
        public void UpgradeCrew(Spaceship spaceship)
        {
            spaceship.crew.level.IncreaseLvl();
        }
    }
}
