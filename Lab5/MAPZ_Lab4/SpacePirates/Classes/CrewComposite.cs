﻿using System;
using System.Collections.Generic;

namespace SpacePirates.Classes
{
    public abstract class Component
    {
        public Component() { }
        public abstract string Operation();
        public virtual void Add(Component member)
        {
            throw new NotImplementedException();
        }
        public virtual void Remove(Component member)
        {
            throw new NotImplementedException();
        }
        public virtual bool IsComposite()
        {
            return true;
        }
    }
    class Member : Component
    {
        public override string Operation()
        {
            return "Member";
        }

        public override bool IsComposite()
        {
            return false;
        }
    }
    class Squadron : Component
    {
        protected List<Component> members = new List<Component>();

        public override void Add(Component member)
        {
            this.members.Add(member);
        }

        public override void Remove(Component member)
        {
            this.members.Remove(member);
        }

        public override string Operation()
        {
            int i = 0;
            string result = "Squadron (";

            foreach (Component component in this.members)
            {
                result += component.Operation();
                if (i != this.members.Count - 1)
                {
                    result += ", ";
                }
                i++;
            }

            return result + ")";
        }
    }
    public class Crew
    {
        public Level level { get; set; } = new Level();
        //public int Level { get; set; }
        public string GetCrewInfo(Component crew)
        {
            return $"{crew.Operation()}\n";
        }

    }
}
