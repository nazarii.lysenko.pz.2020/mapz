﻿using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Diagnostics;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Threading;
using System;

namespace SpacePirates
{
    class Plant
    {
        public int MarginTop { get; set; }
        public int MarginLeft { get; set; }
        public PlantType type { get; set; }
        public Plant(int marginTop, int marginLeft, PlantType type)
        {
            this.MarginTop = marginTop;
            this.MarginLeft = marginLeft;
            this.type = type;
        }
    }
    public interface State
    {
        void GetActive(object sender, RoutedEventArgs e);
        void StopActive() { }
        string GetState();
    }
    class AngryState : State
    {
        Image image;
        public AngryState(Image img)
        {
            image = img;
        }
        public void GetActive(object sender, RoutedEventArgs e)
        {
            image.Visibility = Visibility.Visible;
        }
        public void StopActive()
        {
            
        }
        public string GetState()
        {
            return "angry_";
        }
    }
    class NormalState : State
    {
        public void GetActive(object sender, RoutedEventArgs e) { }
        public void StopActive() { }
        public string GetState()
        {
            return "normal_";
        }
    }

    public class PlantType
    {
        public int Size { get; set; }
        public string Name { get; set; }
        public string ImagePath { get; set; }
        public State State { get; set; }
        public PlantType(int size, string name, string imagePath, State state)
        {
            this.Size = size;
            this.Name = name;
            this.State = state;
            this.ImagePath = imagePath.Insert(imagePath.LastIndexOf('/') + 1, state.GetState());
        }

    }
    public class PlantFactory
    {
        static List<PlantType> plantTypes = new List<PlantType>();
        public static PlantType GetPlantType(int size, string name, string imagePath, State state)
        {

            PlantType type = plantTypes.
                FirstOrDefault(plant => plant.Size == size &&
                      plant.Name == name &&
                      plant.ImagePath == imagePath &&
                      plant.State == state);
            if (type is null)
            {
                type = new PlantType(size, name, imagePath, state);
                plantTypes.Add(type);
            }
            return type;
        }
    }
    class PlantsManager
    {
        public List<Plant> plants { get; set; }
        public PlantsManager()
        {
            plants = new List<Plant>();
        }
        public void AddPlant(
            int marginTop,
            int marginLeft,
            int size,
            string name,
            string imagePath,
            State state)
        {
            PlantType type = PlantFactory.GetPlantType(size, name, imagePath, state);
            Plant newPlant = new Plant(marginTop, marginLeft, type);
            plants.Add(newPlant);
        }
    }
}
