﻿namespace SpacePirates
{
    class Client
    {
        public string GetChestsInfo(IChest chest, int chestsAmount)
        {
            return chestsAmount + " chests: " + chest.getLoot();
        }
    }
    interface IChest
    {
        void fillChest(string filling);
        void fillChest();
        string getLoot();
    }
    class Chest : IChest
    {
        private string filling;
        public Chest()
        {
            this.filling = null;
        }
        public virtual void fillChest(string filling)
        {
            if (this.filling is null)
                this.filling = filling;
            else
                this.filling += ", " + filling;
        }
        public virtual void fillChest()
        {

        }
        public string getLoot()
        {
            return filling;
        }
    }
    class ChestDecorator : IChest
    {
        protected IChest wrappee;
        public ChestDecorator(IChest source)
        {
            this.wrappee = source;
        }
        public virtual void fillChest(string filling)
        {
            this.wrappee.fillChest(filling);
        }
        public virtual void fillChest()
        {

        }
        public string getLoot()
        {
            return this.wrappee.getLoot();
        }
    }
    class FoodDecorator : ChestDecorator
    {
        public FoodDecorator(IChest source) : base(source) { }
        public override void fillChest()
        {
            base.wrappee.fillChest("food");
        }
        public void fillChest(string filling)
        {
            wrappee.fillChest(filling);
        }
    }
    class ArmorDecorator : ChestDecorator
    {
        public ArmorDecorator(IChest source) : base(source) { }
        public override void fillChest()
        {
            base.wrappee.fillChest("armor");
        }
        public void fillChest(string filling)
        {
            wrappee.fillChest(filling);
        }
    }
    class TreasureDecorator : ChestDecorator
    {
        public TreasureDecorator(IChest source) : base(source) { }
        public override void fillChest()
        {
            base.wrappee.fillChest("gold");
        }
        public override void fillChest(string filling)
        {
            wrappee.fillChest(filling);
        }
    }
}
