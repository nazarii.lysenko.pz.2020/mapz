﻿namespace SpacePirates.Classes
{
    public class Spaceship
    {
        public int BlasterLvl { get; set; } = 0;
        public int SpaceshipLvl { get; set; } = 0;
        public Crew crew;

        private Spaceship()
        {
            crew = new Crew();
        }
        private static Spaceship instance;
        public static Spaceship GetInstance()
        {
            if (instance == null)
                instance = new Spaceship();
            return instance;
        }
    }
}
