﻿using System.Collections.Generic;
using System.Linq;

namespace SpacePirates
{
    class Plant
    {
        public int MarginTop { get; set; }
        public int MarginLeft { get; set; }
        public PlantType type { get; set; }
        public Plant(int marginTop, int marginLeft, PlantType type)
        {
            this.MarginTop = marginTop;
            this.MarginLeft = marginLeft;
            this.type = type;
        }
    }
    public class PlantType
    {
        public int Size { get; set; }
        public string Name { get; set; }
        public string ImagePath { get; set; }
        public PlantType(int size, string name, string imagePath)
        {
            this.Size = size;
            this.Name = name;
            this.ImagePath = imagePath;
        }

    }
    public class PlantFactory
    {
        static List<PlantType> plantTypes = new List<PlantType>();
        public static PlantType GetPlantType(int size, string name, string imagePath)
        {
            
            PlantType type = plantTypes.
                FirstOrDefault(plant => plant.Size == size && 
                      plant.Name == name && 
                      plant.ImagePath == imagePath);
            if (type is null)
            {
                type = new PlantType(size, name, imagePath);
                plantTypes.Add(type);
            }
            return type;
        }
    }
    class PlantsManager
    {
        public List<Plant> plants { get; set; }
        public PlantsManager()
        {
            plants = new List<Plant>();
        }
        public void AddPlant(
            int marginTop, 
            int marginLeft,
            int size, 
            string name, 
            string imagePath)
        {
            PlantType type = PlantFactory.GetPlantType(size, name, imagePath);
            Plant newPlant = new Plant(marginTop, marginLeft, type);
            plants.Add(newPlant);
        }
    }
}
