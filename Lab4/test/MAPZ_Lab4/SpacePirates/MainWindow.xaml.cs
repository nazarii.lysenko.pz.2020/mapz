﻿using SpacePirates.Classes;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Media.Imaging;

namespace SpacePirates
{
    public partial class MainWindow : Window
    {
        Stronghold citadel;
        Spaceship spaceship;
        TextBlock textBlock = new TextBlock();
        TextBlock chestBlock = new TextBlock();
        string[] names = { "Frosty", "Cold_Kawun", "Abobus", "Capusta" };
        List<Image> images = new List<Image>();
        Grid[] planetGrids;
        Planet planet;
        Squadron crew = new Squadron();
        public MainWindow()
        {
            InitializeComponent();

            citadel = Stronghold.GetInstance();
            spaceship = Spaceship.GetInstance();
            planetGrids = new Grid[] { gr_pandora, gr_mars, gr_oreo, gr_sirius, gr_blake };

            textBlock.VerticalAlignment = VerticalAlignment.Top;
            textBlock.HorizontalAlignment = HorizontalAlignment.Right;
            textBlock.FontSize = 20;
            textBlock.Foreground = Brushes.WhiteSmoke;

            chestBlock.VerticalAlignment = VerticalAlignment.Top;
            chestBlock.HorizontalAlignment = HorizontalAlignment.Left;
            chestBlock.FontSize = 20;
            chestBlock.Foreground = Brushes.WhiteSmoke;
            chestBlock.Margin = new Thickness(0, 70, 0, 0);

            Squadron squadrone1 = new Squadron();
            Squadron squadrone2 = new Squadron();
            Squadron squadrone3 = new Squadron();

            squadrone1.Add(new Member());
            squadrone1.Add(new Member());

            squadrone2.Add(new Member());
            squadrone2.Add(new Member());
            squadrone2.Add(new Member());

            squadrone3.Add(new Member());

            crew.Add(squadrone1);
            crew.Add(squadrone2);
            crew.Add(squadrone3);
        }
        private void CreatePlants(Grid planetGrid)
        {
            List<int> numberOfPlants = new List<int>();
            string[] paths = { @"../../../frost.png", @"../../../frost_watermelon.png", @"../../../NUT.png", @"../../../capusta.png" };
            Random random = new Random();
            PlantsManager plantsManager = new PlantsManager();
            for (int i = 0; i < 4; i++)
                numberOfPlants.Add(random.Next(1, 4));
            for (int j = 0; j < numberOfPlants.Count; j++)
                for (int i = 0; i < numberOfPlants[j]; i++)
                    plantsManager.AddPlant(random.Next(150, 250), random.Next(-700, 700), random.Next(80, 120), names[j], paths[j]);

            planetGrid.Children.Add(textBlock);
            planetGrid.Children.Add(chestBlock);
            foreach (var item in plantsManager.plants)
            {
                Image plant = new Image();
                plant.Source = new BitmapImage(new Uri(Path.GetFullPath(item.type.ImagePath), UriKind.Absolute));
                plant.Width = item.type.Size;
                plant.Margin = new Thickness(item.MarginLeft, item.MarginTop, 0, 0);
                plant.Name = item.type.Name;
                images.Add(plant);
                planetGrid.Children.Add(plant);
            }


            for (int i = 0; i < numberOfPlants.Count; i++)
            {
                textBlock.Text += plantsManager.plants.Where(plant => plant.type.Name == names[i]).FirstOrDefault().type.Name + ": " + numberOfPlants[i] + "\n";
            }
            Client client = new Client();
            FoodDecorator fd = new FoodDecorator(new Chest());
            fd.fillChest();
            chestBlock.Text += client.GetChestsInfo(fd, planet.Chests / 3) + "\n";

            ArmorDecorator ad = new ArmorDecorator(fd);
            ad.fillChest();
            chestBlock.Text += client.GetChestsInfo(ad, planet.Chests / 3) + "\n";

            TreasureDecorator td = new TreasureDecorator(ad);
            td.fillChest();
            chestBlock.Text += client.GetChestsInfo(td, planet.Chests / 3);

        }
        private void RemovePlants(Grid planetGrid)
        {
            planetGrid.Children.Remove(textBlock);
            textBlock.Text = "";
            chestBlock.Text = "";
            if (planetGrid.Children.Count > 4)
                while (images.Count > 0)
                {
                    planetGrid.Children.Remove(images[0]);
                    images.Remove(images[0]);
                }
        }
        #region Spaceship Upgrades
        private void UpgradeBlaster_Click(object sender, RoutedEventArgs e)
        {
            StrongholdFacade.UpgradeBlasters();
            tb_blaster.Text = spaceship.BlasterLvl.ToString();
        }
        private void UpgradeSpaceship_Click(object sender, RoutedEventArgs e)
        {
            StrongholdFacade.UpgradeSpaceship();
            tb_spaceship.Text = spaceship.SpaceshipLvl.ToString();

        }
        private void UpgradeCrew_Click(object sender, RoutedEventArgs e)
        {
            StrongholdFacade.UpgradeCrew();
            tb_crew.Text = spaceship.crew.Level.ToString();

        }
        private void GetCrewInfo_Click(object sender, RoutedEventArgs e)
        {
            tb_popup.Text = spaceship.crew.GetCrewInfo(crew);
            btn_travel.Visibility = Visibility.Hidden;
            PlanetPopup.IsOpen = true;

        }
        #endregion
        #region Planet Travels
        private void PandoraTravelClick(object sender, RoutedEventArgs e)
        {
            PlanetPopup.IsOpen = false;
            btn_travel.Click -= new RoutedEventHandler(PandoraTravelClick);
            gr_main.Visibility = Visibility.Hidden;
            gr_pandora.Visibility = Visibility.Visible;
            CreatePlants(gr_pandora);
        }
        private void MarsTravelClick(object sender, RoutedEventArgs e)
        {
            btn_travel.Click -= new RoutedEventHandler(MarsTravelClick);
            PlanetPopup.IsOpen = false;
            gr_main.Visibility = Visibility.Hidden;
            gr_mars.Visibility = Visibility.Visible;
            CreatePlants(gr_mars);

        }
        private void SiriusTravelClick(object sender, RoutedEventArgs e)
        {
            btn_travel.Click -= new RoutedEventHandler(SiriusTravelClick);
            PlanetPopup.IsOpen = false;
            gr_main.Visibility = Visibility.Hidden;
            gr_sirius.Visibility = Visibility.Visible;
            CreatePlants(gr_sirius);

        }
        private void BlakeTravelClick(object sender, RoutedEventArgs e)
        {
            btn_travel.Click -= new RoutedEventHandler(BlakeTravelClick);
            PlanetPopup.IsOpen = false;
            gr_main.Visibility = Visibility.Hidden;
            gr_blake.Visibility = Visibility.Visible;
            CreatePlants(gr_blake);
        }
        private void OreoTravelClick(object sender, RoutedEventArgs e)
        {
            btn_travel.Click -= new RoutedEventHandler(OreoTravelClick);
            PlanetPopup.IsOpen = false;
            gr_main.Visibility = Visibility.Hidden;
            gr_oreo.Visibility = Visibility.Visible;
            CreatePlants(gr_oreo);

        }
        #endregion
        #region Planet Info
        private void Pandora_Click(object sender, RoutedEventArgs e)
        {
            HideMarket();
            planet = new Planet(new Pandora());
            RemoveClickEvent();
            btn_travel.Click += new RoutedEventHandler(PandoraTravelClick);
            tb_popup.Text = planet.GetInfo();
            PlanetPopup.IsOpen = true;
        }
        private void Oreo_Click(object sender, RoutedEventArgs e)
        {
            HideMarket();
            planet = new Planet(new Oreo());
            RemoveClickEvent();
            btn_travel.Click += new RoutedEventHandler(OreoTravelClick);
            tb_popup.Text = planet.GetInfo();
            PlanetPopup.IsOpen = true;
        }
        private void Mars_Click(object sender, RoutedEventArgs e)
        {
            HideMarket();
            planet = new Planet(new Mars());
            RemoveClickEvent();
            btn_travel.Click += new RoutedEventHandler(MarsTravelClick);
            tb_popup.Text = planet.GetInfo();
            PlanetPopup.IsOpen = true;
        }
        private void Sirius_Click(object sender, RoutedEventArgs e)
        {
            HideMarket();
            planet = new Planet(new Sirius());
            RemoveClickEvent();
            btn_travel.Click += new RoutedEventHandler(SiriusTravelClick);
            tb_popup.Text = planet.GetInfo();
            PlanetPopup.IsOpen = true;
        }
        private void Blake_Click(object sender, RoutedEventArgs e)
        {
            HideMarket();
            planet = new Planet(new Blake());
            RemoveClickEvent();
            btn_travel.Click += new RoutedEventHandler(BlakeTravelClick);
            tb_popup.Text = planet.GetInfo();
            PlanetPopup.IsOpen = true;
        }
        #endregion
        private void Stronghold_Click(object sender, RoutedEventArgs e)
        {
            sp_upgrades.Visibility = Visibility.Visible;
            tb_title.Visibility = Visibility.Visible;
        }
        #region Support Methods
        private void HideMarket()
        {
            sp_upgrades.Visibility = Visibility.Hidden;
            tb_title.Visibility = Visibility.Hidden;
        }
        private void Cancel_Click(object sender, RoutedEventArgs e)
        {
            PlanetPopup.IsOpen = false;
            btn_travel.Visibility = Visibility.Visible;
        }
        private void Back_Click(object sender, RoutedEventArgs e)
        {
            PlanetPopup.IsOpen = false;
            gr_main.Visibility = Visibility.Visible;

            foreach (var item in planetGrids)
            {
                item.Visibility = Visibility.Hidden;
                item.Children.Remove(textBlock);
                item.Children.Remove(chestBlock);
                RemovePlants(item);
            }
        }
        private void RemoveClickEvent()
        {
            btn_travel.Click -= new RoutedEventHandler(PandoraTravelClick);
            btn_travel.Click -= new RoutedEventHandler(MarsTravelClick);
            btn_travel.Click -= new RoutedEventHandler(SiriusTravelClick);
            btn_travel.Click -= new RoutedEventHandler(BlakeTravelClick);
            btn_travel.Click -= new RoutedEventHandler(OreoTravelClick);

        }
        #endregion
    }
}
