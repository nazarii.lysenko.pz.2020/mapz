﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SpeedTests
{
    class Program
    {
        static void Main(string[] args)
        {
            Stopwatch sw = new Stopwatch();
            Stopwatch d = new Stopwatch();
            ToTest t = new ToTest();
            sw.Start();
            for (int i = 0; i < 10000000; i++)
                t.LongLoop();
            sw.Stop();
            for (int i = 0; i < 10000000; i++)
            {
                using (new ToTest()) { d.Start(); }
                d.Stop();
            }
            Console.WriteLine("Work: " + sw.ElapsedMilliseconds);
            Console.WriteLine("Dispose: " + d.ElapsedMilliseconds);
        }
        struct ToTest : IDisposable
        {
            int test1;
            int test2;
            int test3;
            int test4;
            int test5;
            public void Dispose()
            {
            }
            public void LongLoop()
            {
                test1 = 1;
                test2 = 2;
                test3 = 3;
                test4 = 4;
                test5 = 5;
                int a = 0;
                for (int i = 0; i < 100; i++)
                {
                    a = a == 1 ? a - 1 : a + 1;
                }
            }
        }
    }
}
