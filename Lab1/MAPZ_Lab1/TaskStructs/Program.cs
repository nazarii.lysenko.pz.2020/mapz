﻿//using System;
//using System.Collections.Generic;
//using System.Linq;
//using System.Text;
//using System.Threading.Tasks;

//namespace Task01
//{
//    class Program
//    {
//        protected struct Test { }

//        private Test var;

//        static void Main(string[] args)
//        {
//            //Console.WriteLine(TestEnum.A == TestEnum.B && TestEnum.A == TestEnum.A);
//            //Console.WriteLine(TestEnum.A == TestEnum.B || TestEnum.A == TestEnum.A);
//            //Console.WriteLine(TestEnum.A & TestEnum.B);
//            //Console.WriteLine(TestEnum.A | TestEnum.B);
//            //Console.WriteLine(TestEnum.A ^ TestEnum.B);

//            //StaticVars sv = new StaticVars();
//            //sv.Print();

//            //TestClass tc = new TestClass();
//            //string t = "test";
//            //Console.WriteLine(t);
//            //tc.ReturnString(out t);
//            //Console.WriteLine(t);

//            //int a = 1; 
//            //object obj = a; //boxing

//            //int a1 = (int)obj; //unboxing

//            int a = 5;
//            double b = a; //implicit
//            short c = (short)a; //explicit
//        }
//    }


//    enum TestEnum
//    {
//        A = 1,
//        B,
//        C,
//        D,
//        E
//    }
//    struct TestStruct
//    {
//        int var;
//        void Test() { }
//    }
//    interface IFirstTestInterface
//    {
//        void SomeWork();
//    }
//    interface ISecondTestInterface
//    {
//        void EvenMoreWork();
//    }
//    abstract struct TestAbstractClass : IFirstTestInterface, ISecondTestInterface
//    {
//        public TestAbstractClass()
//        {

//        }
//        public TestAbstractClass(int a)
//        {

//        }

//        void IFirstTestInterface.SomeWork()
//        {
//            Console.WriteLine("Some work!");
//        }
//        void ISecondTestInterface.EvenMoreWork()
//        {
//            Console.WriteLine("More work!");
//        }
//        public abstract void MoreWork();

//        #region AccessModifiers
//        public void TestPublic() { }
//        private void TestPrivate() { }
//        protected void TestProt() { }
//        internal void TestInter() { }
//        internal protected void TestInterProt() { }
//        #endregion
//    }
//    struct TestClass : TestAbstractClass
//    {
//        int var;
//        void Test() { }
//        #region Constructors
//        public TestClass()
//        {

//        }
//        public TestClass(int a)
//        {
//            this.var = a;
//        }
//        public TestClass(int a, int b) : base(b)
//        {
//            this.var = a;
//        }
//        #endregion

//        public override void MoreWork()
//        {
//            Console.WriteLine("Abstract work!");
//        }
//        public void Print()
//        {
//            Console.WriteLine("Test");
//        }
//        public void Print(string text)
//        {
//            Console.WriteLine(text);
//        }
//        public void ChangeString(/*ref*/ string text)
//        {
//            text = text.ToUpper();
//        }
//        public void ReturnString(out string text)
//        {
//            text = "Text";
//        }
//    }
//    struct A { }
//    struct B : A { }

//    struct StaticVars
//    {
//        static int var;
//        int test;
//        public StaticVars()
//        {
//            var = 0;
//            test = 0;
//        }
//        static StaticVars()
//        {
//            var = 1;
//            //test = 1;
//        }
//        public void Print()
//        {
//            Console.WriteLine($"var = {var}\ntest = {test}");
//        }
//    }
//}
