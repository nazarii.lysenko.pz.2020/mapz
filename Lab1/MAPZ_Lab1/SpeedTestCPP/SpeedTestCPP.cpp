#include <iostream>
#include <chrono>

struct ToTest
{
	int test1;
	int test2;
	int test3;
	int test4;
	int test5;
	void LongLoop()
	{
		test1 = 1;
		test2 = 2;
		test3 = 3;
		test4 = 4;
		test5 = 5;
		int a = 0;
		for (int i = 0; i < 100; i++)
		{
			a = a == 1 ? a - 1 : a + 1;
		}
	}
};
int main()
{
	
	ToTest* t = (struct ToTest*)malloc(sizeof(struct ToTest));
	auto start = std::chrono::high_resolution_clock::now();
	for (int i = 0; i < 10000000; i++)
		t->LongLoop();
	auto finish = std::chrono::high_resolution_clock::now();
	auto milliseconds = std::chrono::duration_cast<std::chrono::milliseconds>(finish - start);
	std::cout << "Work: " << milliseconds.count() << "ms\n";
	milliseconds = milliseconds.zero();
	auto microseconds = std::chrono::microseconds::zero();
	for (int i = 0; i < 10000000; i++)
	{
		ToTest* t1 = (struct ToTest*)malloc(sizeof(struct ToTest));
		auto start1 = std::chrono::high_resolution_clock::now();
		delete t1;
		auto finish1 = std::chrono::high_resolution_clock::now();
		//auto milliseconds1 = std::chrono::duration_cast<std::chrono::microseconds>(finish1 - start1);
		microseconds += std::chrono::duration_cast<std::chrono::microseconds>(finish1 - start1);
	}
	std::cout << "Disposing: " << microseconds.count() / 100 << "ms\n";

}
